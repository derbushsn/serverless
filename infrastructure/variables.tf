variable "aws_access_key" {
  description = "AWS Access Key"
}

variable "aws_secret_key" {
  description = "AWS Secret Key"
}

variable "aws_region" {
  default     = "us-east-1"
  description = "AWS Region"
}

variable "s3_bucket_lambda" {
  description = "S3 Bucket wit lambda"
}

variable "lambda_version" {
  description = "Lambda version"
}