provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

module "iam" {
  source = "./modules/iam"

  dynamodb-table-arn = module.dynamo_db.app-dynamodb-table-arn
}

module "lambda" {
  source = "./modules/lambda"

  role = module.iam.app-lambda-role
  s3_bucket_lambda = var.s3_bucket_lambda
  lambda_version = var.lambda_version
}

module "api_gateway" {
  source = "./modules/api_gateway"

  app-lambda-invoke-arn = module.lambda.app-lambda-invoke-arn
  app-lambda-function-name = module.lambda.app-lambda-function-name
}

module "dynamo_db" {
  source = "./modules/dynamo_db"
}