variable "app-lambda-invoke-arn" {
  description = "Lambda invoke arn"
}

variable "app-lambda-function-name" {
  description = "Lambda function name"
}
