variable "role" {
  description = "IAM Role"
}

variable "lambda_version" {
  description = "lambda version"
}

variable "s3_bucket_lambda" {
  description = "S3 Bucket wit lambda"
}