resource "aws_lambda_function" "app-get" {
  function_name = "ServerlessAppGet"

  # The bucket name as created earlier with "aws s3api create-bucket"
  s3_bucket = var.s3_bucket_lambda
  s3_key    = "v${var.lambda_version}/lambda.zip"

  # "main" is the filename within the zip file (main.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "main.handler"
  runtime = "nodejs10.x"

  role = var.role
}