output "app-lambda-invoke-arn" {
  value = aws_lambda_function.app-get.invoke_arn
}

output "app-lambda-function-name" {
  value = aws_lambda_function.app-get.function_name
}
