resource "aws_dynamodb_table" "app-dynamodb-table" {
  name = "AppGet"
  read_capacity = 5
  write_capacity = 5
  hash_key = "Series"
  range_key = "Number"

  attribute {
    name = "Series"
    type = "S"
  }

  attribute {
    name = "Number"
    type = "S"
  }
}