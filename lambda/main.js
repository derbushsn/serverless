const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

exports.handler = function (event, context, callback) {
  let series = event.queryStringParameters && event.queryStringParameters.series || null;
  let number = event.queryStringParameters && event.queryStringParameters.number || null;
  const getResponse = function (isExpired) {
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({
        is_expired: isExpired
      }),
    }
  };
  const existsResponse = getResponse(true);
  const noExistResponse = getResponse(false);

  if (series === null || number === null) {
    return callback(null, noExistResponse);
  }

  var params = {
    Key: {
      "Series": {
        S: series
      },
      "Number": {
        S: number
      }
    },
    TableName: "AppGet"
  };
  try {
    dynamodb.getItem(params, function (err, data) {
      if (err) {
        callback(null, noExistResponse);
      } else {
        callback(null, existsResponse);
      }
    });
  } catch (e) {
    callback(null, noExistResponse);
  }
}