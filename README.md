

# Terraform for AWS API Gateway + Lambda + DynamoDB

Run:

    cp infrastructure/app.dist.tfvars infrastructure/app.tfvars
    
Add your aws_access_key, aws_secret_key, s3_bucket_lambda, lambda_version to `infrastructure/app.tfvars`

Prepare lambda:

    cd lambda
    npm install
    zip lambda.zip node_modules main.js
    aws s3api create-bucket --bucket {s3_bucket_lambda} --region us-east-1
    aws s3 cp lambda.zip s3://{s3_bucket_lambda}}/v{lambda_version}/lambda.zip

Run terraform:

    terraform init
    terraform apply -var-file="app.tfvars"
   
 see `base_url` in output
    
API GET:`{base_url}/api?series=8210&number=996305`
    
PUT item to DynamoDB:
    
    aws dynamodb put-item \
        --table-name AppGet \
        --item '{"Series":{"S":"8210"},"Number":{"S":"996305"}}'